var util = require('util')
var child_process = require('child_process')
var fs = require('fs')
let dotenv = require('dotenv');
const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)
const exec = util.promisify(child_process.exec)
dotenv.config('./.env')
const temp_dir = process.env.TEMP_RUNING_DIR
const pem = process.env.PEM;
const project_dir = process.env.PROJECT_DIR;
const build_dir = project_dir + '/build';
if (!temp_dir) {
    throw "No temp_dir"
}
if (!pem) {
    throw "No pem"
}
const main = async () => {
    await clear_temp()
    await build();
    console.log('编译完成')
    await copy_down();
    console.log('拉取配置文件完成')
    const source = (await
        readFile(`${temp_dir}/yennefer-web-order`,
            { encoding: 'utf-8' }
        )
    )
        .toString()
    const old = source.match(/yennefer-order(\S+);/g)[0];
    const version = 'yennefer-order' + await modify_version(source) + ';';
    console.log('replace: ',
        '\x1B[35m', old, '\x1B[39m',
        ' to ',
        '\x1B[32m', version, '\x1B[39m'
    );
    const new_source = source.replace(old, version)
    const file_dir_name = version.substring(0, version.length - 1);
    await exec(`rm ./temp/yennefer-web-order`);
    await writeFile('./temp/yennefer-web-order', new_source, { 'encoding': 'utf-8' });
    await exec(`cp -r ${build_dir} ./temp/${file_dir_name}`);

    await exec(`scp -i ${pem} -r ./temp/${file_dir_name} root@8.209.81.113:/var/www`)
    console.log('上传编译文件夹完成')

    await exec(`scp -i ${pem} ./temp/yennefer-web-order root@8.209.81.113:/etc/nginx/sites-available/yennefer-web-order`);
    await exec(`./restart_nginx.sh \"/var/www/${file_dir_name}\"`);
    await clear_temp();
    console.log('结束')
}
async function modify_version(str) {
    const now = new Date();
    let new_version = now.getFullYear().toString() + (now.getMonth() + 1).toString() + now.getDate().toString();
    const old_date = str.match(/(?<=yennefer-order)\d+/g)[0];
    console.log(
        'old-date: ', old_date,
        '\ntoday:    ', new_version
    )
    if (old_date === new_version) {
        if (str.match(/yennefer-order\d+-/g)[0]) {
            const today_version = str.match(/\d+(?=;)/g)[0];
            console.log('today_version:          ', today_version)
            const new_sub_version = Number(today_version) + 1;
            console.log('set new sub_version:    ', new_sub_version);
            new_version += '-' + new_sub_version.toString();
        }
    } else {
        new_version += '-1';
    }
    console.log(
        'final version here:    ',
        '\x1B[32m', new_version, '\x1B[39m',
    );
    return new_version;
}
async function copy_down() {
    const cmd = `scp -i \"${pem}\" root@8.209.81.113:/etc/nginx/sites-available/yennefer-web-order ./temp/yennefer-web-order`
    console.log('cmd: ', cmd);
    await exec(cmd);
}
async function build() {
    return new Promise((suc, fail) => {
        const b = child_process.spawn(`yarn`, ['build:dev'], { cwd: project_dir });
        b.stdout.on('data', function (data) {
            console.log('stdout: ' + data.toString());
        });
        b.stderr.on('data', function (data) {
            console.log('stderr: ' + data.toString());
        });
        b.on('exit', function (code) {
            console.log('child process exited with code ' + code.toString());
            suc()
        });
    })
}
async function clear_temp() {
    await exec(`rm -rf ./temp/*`)
}
main()