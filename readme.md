## 先配置, 再运行

* .env
    - 打开.env
    - 配置本地web-order路径
    - 配置pem文件路径（git仓库是没有上传pem）

* 选用合适的终端需要满足一下命令:
    - sh 
    - cp 
    - scp
    - rm
    - ssh
    > (自行安装)
    

* 需对应平台修改代码 Windows / MacOs
    - 比如 Windows 路径使用 \ 分隔 ， macOS使用 /

    - 按自身需求 创建新的git分支，比如 windows branch 或 macOS branch，完备好就PR
    
